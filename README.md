# lyfe
A Python web implementation of Conway's game of life.
Disclaimer : I did not put much effort into making the web page compatible with old browsers, so... just update yours if the UI looks broken.

You can try it here => http://lyfe-web.herokuapp.com/

## Execution
```
. venv/bin/activate
python app/web.py
```

## Requirements
Python 3.x is required.
Please setup your virtualenv with Python 3 and install the requirements using the ```requirements.txt``` file provided.
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```
