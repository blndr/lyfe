import os

import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
app_dir = parent_dir + '/app'
sys.path.append(app_dir)
