import json
from unittest.mock import patch

from web import app


class ApiTest:
    def setup_method(self):
        app.config['TESTING'] = True
        self.app = app.test_client(self)

    def test_evolve_returns_the_evolutions_of_cells(self):
        # given
        cells = [
            {'x': 0, 'y': 0, 'alive': False},
            {'x': 1, 'y': 0, 'alive': True},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 0, 'y': 1, 'alive': True},
            {'x': 1, 'y': 1, 'alive': True},
            {'x': 2, 'y': 1, 'alive': False}
        ]

        # when
        response = self.app.post('/api/grid/evolve', data=json.dumps(cells), content_type='application/json')

        # then
        body = json.loads(response.get_data(as_text=True))
        assert body == [{'x': 0, 'y': 0, 'alive': True}]

    @patch('web.init_grid_from_template')
    @patch('web.to_cell_array')
    def test_cells_from_template_returns_cells_corresponding_to_given_template(self, to_cell_array,
                                                                               init_grid_from_template):
        # given
        init_grid_from_template.return_value = [['first_row'], ['second_row']]
        to_cell_array.return_value = [{'x': 0, 'y': 0, 'alive': True}]

        # when
        response = self.app.get('/api/grid?template=lwss', content_type='application/json')

        # then
        body = json.loads(response.get_data(as_text=True))
        assert body == [{'x': 0, 'y': 0, 'alive': True}]
