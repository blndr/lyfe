from unittest.mock import patch

from engine.game import alive_neighbours, alive_next, evolve, pretty, init_grid_from_template
from engine.state import State as s


class GameTest:
    def test_alive_neighbours_returns_zero_when_eight_dead_surrounding(self):
        # given
        grid = [[s.dead, s.dead, s.dead], [s.dead, s.dead, s.dead], [s.dead, s.dead, s.dead]]
        x, y = 1, 1

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 0

    def test_alive_neighbours_returns_three_when_eight_surrounding(self):
        # given
        grid = [[s.alive, s.dead, s.dead], [s.dead, s.dead, s.alive], [s.dead, s.dead, s.alive]]
        x, y = 1, 1

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 3

    def test_alive_neighbours_returns_eight_when_eight_surrounding(self):
        # given
        grid = [[s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive]]
        x, y = 1, 1

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 8

    def test_alive_neighbours_returns_five_when_five_surrounding_from_left_column(self):
        # given
        grid = [[s.alive, s.alive, s.dead, s.alive], [s.dead, s.alive, s.dead, s.alive],
                [s.alive, s.alive, s.dead, s.alive]]
        x, y = 0, 1

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 5

    def test_alive_neighbours_returns_five_when_five_surrounding_from_top_row(self):
        # given
        grid = [[s.alive, s.dead, s.alive, s.dead], [s.alive, s.alive, s.alive, s.dead],
                [s.alive, s.alive, s.alive, s.dead]]
        x, y = 1, 0

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 5

    def test_alive_neighbours_returns_five_when_five_surrounding_from_right_column(self):
        # given
        grid = [[s.alive, s.dead, s.alive, s.alive], [s.dead, s.dead, s.alive, s.dead],
                [s.alive, s.dead, s.alive, s.alive]]
        x, y = 3, 1

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 5

    def test_alive_neighbours_returns_five_when_five_surrounding_from_bottom_row(self):
        # given
        grid = [[s.alive, s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive, s.dead],
                [s.alive, s.dead, s.alive, s.dead]]
        x, y = 1, 2

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 5

    def test_alive_neighbours_returns_three_when_three_surrounding_top_left_corner(self):
        # given
        grid = [[s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive]]
        x, y = 0, 0

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 3

    def test_alive_neighbours_returns_three_when_three_surrounding_top_right_corner(self):
        # given
        grid = [[s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive]]
        x, y = 2, 0

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 3

    def test_alive_neighbours_returns_three_when_three_surrounding_bottom_right_corner(self):
        # given
        grid = [[s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive]]
        x, y = 2, 2

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 3

    def test_alive_neighbours_returns_three_when_three_surrounding_bottom_left_corner(self):
        # given
        grid = [[s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive], [s.alive, s.alive, s.alive]]
        x, y = 0, 2

        # when
        living = alive_neighbours(grid, x, y)

        # then
        assert living == 3

    @patch('engine.game.alive_neighbours')
    def test_alive_next_returns_true_if_given_cell_has_exactly_three_alive_neighbours(self, alive_neighbours):
        # given
        grid = [['any_grid']]
        alive_neighbours.return_value = 3

        # then
        assert alive_next(grid, 0, 0) is True

    @patch('engine.game.alive_neighbours')
    def test_alive_next_returns_false_if_given_cell_has_less_than_two_alive_neighbours(self, alive_neighbours):
        # given
        grid = [['any_grid']]
        alive_neighbours.return_value = 1

        # then
        assert alive_next(grid, 0, 0) is False

    @patch('engine.game.alive_neighbours')
    def test_alive_next_returns_false_if_given_cell_has_more_than_three_alive_neighbours(self, alive_neighbours):
        # given
        grid = [['any_grid']]
        alive_neighbours.return_value = 4

        # then
        assert alive_next(grid, 0, 0) is False

    @patch('engine.game.alive_neighbours')
    def test_alive_next_returns_true_if_given_cell_is_alive_and_has_exactly_two_alive_neighbours(self,
                                                                                                 alive_neighbours):
        # given
        grid = [[s.alive, s.dead], [s.alive, s.alive]]
        alive_neighbours.return_value = 2

        # then
        assert alive_next(grid, 0, 0) is True

    @patch('engine.game.alive_neighbours')
    def test_alive_next_returns_false_if_given_cell_is_dead_and_has_exactly_two_alive_neighbours(self,
                                                                                                 alive_neighbours):
        # given
        grid = [[s.dead, s.dead], [s.alive, s.alive]]
        alive_neighbours.return_value = 2

        # then
        assert alive_next(grid, 0, 0) is False

    def test_evolve_returns_a_new_grid_by_calculating_if_each_cell_is_alive_next(self):
        # given
        grid = [[s.dead, s.alive, s.dead], [s.dead, s.alive, s.dead], [s.dead, s.alive, s.dead]]

        # when
        new_grid = evolve(grid)

        # then
        assert new_grid == [[s.dead, s.dead, s.dead], [s.alive, s.alive, s.alive], [s.dead, s.dead, s.dead]]

    def test_pretty_returns_a_rectangle_string_representation_of_a_grid(self):
        # given
        grid = [
            [s.dead, s.alive, s.alive, s.dead],
            [s.alive, s.alive, s.dead, s.alive],
            [s.dead, s.dead, s.alive, s.dead],
            [s.alive, s.dead, s.alive, s.dead]
        ]

        # when
        representation = pretty(grid)

        # then
        assert representation == '' \
                                 ' ---- \n' \
                                 '| oo |\n' \
                                 '|oo o|\n' \
                                 '|  o |\n' \
                                 '|o o |\n' \
                                 ' ---- \n'

    def test_init_grid_from_template_returns_a_two_dimensional_grid_from_a_string(self):
        # given
        template = '' \
                   ' oo\n' \
                   'oo \n' \
                   '  o\n' \
                   '   '

        # when
        grid = init_grid_from_template(template)

        # then
        assert grid == [
            [s.dead, s.alive, s.alive],
            [s.alive, s.alive, s.dead],
            [s.dead, s.dead, s.alive],
            [s.dead, s.dead, s.dead]
        ]
