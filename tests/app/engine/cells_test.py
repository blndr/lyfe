from engine.cells import to_grid, to_cell_array, evolutions, dimensions
from engine.state import State as s


class ConverterTest:
    def to_grid_returns_a_grid_from_a_cell_array(self):
        # given
        cells = [
            {'x': 0, 'y': 0, 'alive': True},
            {'x': 1, 'y': 0, 'alive': False},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 0, 'y': 1, 'alive': False},
            {'x': 1, 'y': 1, 'alive': True},
            {'x': 2, 'y': 1, 'alive': True}
        ]
        width, height = 3, 2

        # when
        grid = to_grid(cells, width, height)

        # then
        assert grid == [
            [s.alive, s.dead, s.dead],
            [s.dead, s.alive, s.alive]
        ]

    def test_to_cell_array_returns_a_cell_array_from_a_grid(self):
        # given
        grid = [
            [s.alive, s.dead, s.dead],
            [s.dead, s.alive, s.alive]
        ]

        # when
        cells = to_cell_array(grid)

        # then
        assert cells == [
            {'x': 0, 'y': 0, 'alive': True},
            {'x': 1, 'y': 0, 'alive': False},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 0, 'y': 1, 'alive': False},
            {'x': 1, 'y': 1, 'alive': True},
            {'x': 2, 'y': 1, 'alive': True}
        ]

    def test_updates_returns_only_modified_cells_given_two_cells_array(self):
        # given
        cells = [
            {'x': 0, 'y': 0, 'alive': False},
            {'x': 1, 'y': 0, 'alive': False},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 2, 'y': 1, 'alive': False}
        ]

        evolution = [
            {'x': 0, 'y': 0, 'alive': False},
            {'x': 1, 'y': 0, 'alive': True},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 2, 'y': 1, 'alive': True}
        ]

        # when
        updates = evolutions(cells, evolution)

        # then
        assert updates == [
            {'x': 1, 'y': 0, 'alive': True},
            {'x': 2, 'y': 1, 'alive': True}
        ]

    def test_dimensions_should_return_the_width_and_height_of_a_cell_array_from_max_x_and_y(self):
        # given
        # given
        cells = [
            {'x': 0, 'y': 0, 'alive': False},
            {'x': 1, 'y': 0, 'alive': False},
            {'x': 0, 'y': 3, 'alive': False},
            {'x': 2, 'y': 0, 'alive': False}
        ]

        # when
        width, height = dimensions(cells)

        # then
        assert width == 3
        assert height == 4
