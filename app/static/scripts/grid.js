function getCells() {
    var table = [];
    $('td.cell').each(function (index, cell) {
        var $cell = $(cell);
        table.push({x: $cell.data('x'), y: $cell.data('y'), alive: $cell.hasClass('alive')});
    });
    return table;
}

function fillTable(cells) {
    for (cell of cells) {
        var $cell = $('.cell[data-x=' + cell.x + '][data-y=' + cell.y + ']');
        $cell.toggleClass('alive', cell.alive);
    }
}

function clearGrid() {
    $('td.cell').each(function (index, cell) {
        $(cell).removeClass('alive');
    });
}

function evolve() {
    var url = '/api/grid/evolve',
        cells = getCells();

    loading('start');
    $.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(cells)
    }).done(function (response) {
        fillTable(response);
        if (autoplay) {
            setTimeout(evolve, 1000);
        }
    }).always(function () {
        loading('stop');
    });
}

function loading(state) {
    $('#loading').attr('src', 'static/images/loading-' + state + '.svg');
}

function getTemplate(template) {
    loading('start');
    $.ajax({
        type: 'GET',
        url: '/api/grid?template=' + template,
        contentType: 'application/json'
    }).done(function (response) {
        console.log(response);
        fillTable(response);
    }).always(function () {
        loading('stop');
    });
}

var HEIGHT = 20,
    WIDTH = 20,
    autoplay = false;

var grid = {
    bindAliveToggling: function () {
        $('td.cell').on('click', function (event) {
            $target = $(event.currentTarget);
            $target.toggleClass('alive');
        });
    },

    initTable: function () {
        var rows = '';
        for (var i = 0; i < HEIGHT; i++) {
            var columns = '';
            for (var j = 0; j < WIDTH; j++) {
                columns += '<td data-x="' + j + '" data-y="' + i + '" class="cell"></td>';
            }
            rows += '<tr>' + columns + '</tr>';
        }

        $('#grid').html(rows);
    },

    bindEvolveButton: function () {
        $('#evolve').on('click', function () {
            evolve();
        });
    },

    bindSpacebar: function () {
        $('body').on('keypress', function (event) {
            if (event.which === 32) {
                event.preventDefault();
                evolve();
            }
        });
    },

    bindAutoplay: function () {
        $('#autoplay').on('click', function (event) {
            var $button = $(event.currentTarget);
            $button.toggleClass('active');
            if ($button.hasClass('active')) {
                autoplay = true;
                evolve();
                $('#evolve').addClass('disabled');
                $('#clear').addClass('disabled');
            } else {
                autoplay = false;
                $('#evolve').removeClass('disabled');
                $('#clear').removeClass('disabled');
            }
        });
    },

    bindClearGrid: function () {
        $('#clear').on('click', function () {
            clearGrid();
        });
    },

    bindTemplate: function () {
        $('.template').on('click', function (event) {
            var template = $(event.currentTarget).data('template');
            getTemplate(template);
        })
    }
};

$(document).ready(function () {
    grid.initTable();
    grid.bindSpacebar();
    grid.bindAliveToggling();
    grid.bindEvolveButton();
    grid.bindAutoplay();
    grid.bindClearGrid();
    grid.bindTemplate();
});