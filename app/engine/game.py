from engine.state import State


def init_grid_from_template(template):
    lines = template.split('\n')
    height = len(lines)
    width = len(lines[0])

    blank_grid = init_grid(width, height)

    for y in range(height):
        blank_grid[y] = [State.alive if cell == State.alive.value else State.dead for cell in lines[y]]

    return blank_grid


def init_grid(width, height):
    return [[State.dead for i in range(width)] for j in range(height)]


def alive_neighbours(grid, x, y):
    neighbours = []

    if x > 0:
        neighbours.append(grid[y][x - 1])

    if y > 0:
        neighbours.append(grid[y - 1][x])

    if x > 0 and y > 0:
        neighbours.append(grid[y - 1][x - 1])

    if x > 0 and _next_row(grid, y):
        neighbours.append(grid[y + 1][x - 1])

    if y > 0 and _next_col(grid, x, y):
        neighbours.append(grid[y - 1][x + 1])

    if _next_col(grid, x, y):
        neighbours.append(grid[y][x + 1])

    if _next_col(grid, x, y) and _next_row(grid, y):
        neighbours.append(grid[y + 1][x + 1])

    if _next_row(grid, y):
        neighbours.append(grid[y + 1][x])

    return len(_filter_dead(neighbours))


def alive_next(grid, x, y):
    living = alive_neighbours(grid, x, y)

    if living == 3:
        return True

    if living == 2:
        return grid[y][x] == State.alive

    return False


def evolve(grid):
    width, height = _dimensions(grid)
    new_grid = init_grid(width, height)

    for y in range(height):
        for x in range(width):
            next_state = State.alive if alive_next(grid, x, y) else State.dead
            new_grid[y][x] = next_state

    return new_grid


def pretty(grid):
    width, height = _dimensions(grid)

    representation = ' ' + '-' * width + ' \n'
    for y in range(height):
        representation += '|'

        for x in range(width):
            representation += grid[y][x].value

        representation += '|\n'
    representation += ' ' + '-' * width + ' \n'

    return representation


def _next_row(grid, y):
    return y < len(grid) - 1


def _next_col(grid, x, y):
    return x < len(grid[y]) - 1


def _filter_dead(neighbours):
    return [cell for cell in neighbours if cell == State.alive]


def _dimensions(grid):
    return len(grid[0]), len(grid)
