from enum import Enum


class State(Enum):
    dead = ' '
    alive = 'o'
