from engine.state import State as s


def to_grid(cells, width, height):
    grid = [[s.dead for i in range(width)] for j in range(height)]

    for cell in cells:
        x, y = cell['x'], cell['y']
        grid[y][x] = s.alive if cell['alive'] else s.dead

    return grid


def to_cell_array(grid):
    width, height = len(grid[0]), len(grid)
    cells = []

    for y in range(height):
        for x in range(width):
            alive = grid[y][x] == s.alive
            cells.append({'x': x, 'y': y, 'alive': alive})

    return cells


def evolutions(cells, evolution):
    return [cell for cell in evolution if cell not in cells]


def dimensions(cells):
    return max([cell['x'] for cell in cells]) + 1, max([cell['y'] for cell in cells]) + 1
