import json
import os

from flask import Flask, jsonify, request
from flask import render_template

from engine.cells import dimensions, to_grid, to_cell_array, evolutions
from engine.game import evolve, init_grid_from_template

app = Flask(__name__)


@app.route('/')
def index():
    templates = [
        {'name': 'Glider (vessel)', 'code': 'glider'},
        {'name': 'Lightweight spaceship (vessel)', 'code': 'lwss'},
        {'name': 'Pulsar (period 3)', 'code': 'pulsar'},
        {'name': 'Pentadecathlon (period 15)', 'code': 'penta'}
    ]
    return render_template('index.html', templates=templates)


@app.route('/api/grid/evolve', methods=['POST'])
def evolve_grid():
    cells = json.loads(request.get_data(as_text=True))
    width, height = dimensions(cells)
    grid = to_grid(cells, width, height)
    new_grid = evolve(grid)
    new_cells = to_cell_array(new_grid)
    return jsonify(evolutions(cells, new_cells))


@app.route('/api/grid', methods=['GET'])
def cells_from_template():
    template = request.args.get('template')
    with open('app/grids/' + template + '.grid', 'r') as f:
        grid = init_grid_from_template(f.read())
    cells = to_cell_array(grid)
    return jsonify(cells)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
